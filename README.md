# What's this?

This is a barebones example project which demonstrates how to use [Karma test runner](https://karma-runner.github.io/1.0/index.html) with [Webpack](https://webpack.js.org/) (and [Jasmine](https://jasmine.github.io/)). It was created after reading a blog post by Joe Attardi named How to set up [Karma and Jasmine with webpack](http://www.thinksincode.com/2016/07/07/karma-jasmine-webpack.html).

# How to use

1. clone this project on your local machine
2. cd to the project directory and do the `npm install`
3. install karma-cli globally (`npm install -g karma-cli`)
4. run Karma (`karma start`)